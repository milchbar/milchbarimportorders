<?php

class Shopware_Plugins_Backend_MilchbarImportOrders_Bootstrap extends Shopware_Components_Plugin_Bootstrap
{

    public function getVersion()
    {
        return '0.8';
    }

    public function getLabel()
    {
        return 'Import Order from JSON';
    }

    public function getInfo()
    {
        return array(
            'version' => $this->getVersion(),
            'label' => $this->getLabel(),
            'supplier' => 'Milchbar',
            'support' => 'Milchbar',
            'autor' => 'Milchbar',
            'copyright' => 'Copyright &copy; 2017, Milchbar',
            'link' => 'http://www.milchbar.nl'
        );
    }

    public function getCapabilities()
    {
        return array(
            'install' => true,
            'update' => true,
            'enable' => true
        );
    }

    public function install()
    {
        $this->registerEvents();
        $this->createConfig();

        return array('success' => true, 'invalidateCache' => array('backend'));
    }

    public function update($version)
    {
        $this->registerEvents();

        return array('success' => true, 'invalidateCache' => array('backend'));
    }

    public function uninstall()
    {
        return array('success' => true, 'invalidateCache' => array('backend'));
    }

    private function registerEvents()
    {
        $this->subscribeEvent(
            'Shopware_Console_Add_Command',
            'onAddConsoleCommand'
        );

        $this->subscribeEvent(
            'Enlight_Controller_Front_DispatchLoopStartup',
            'onStartDispatch'
        );
    }

    /**
     * This callback function is triggered at the very beginning of the dispatch process and allows
     * us to register additional events on the fly. This way you won't ever need to reinstall you
     * plugin for new events - any event and hook can simply be registerend in the event subscribers
     */
    public function onStartDispatch(Enlight_Event_EventArgs $args)
    {
        $this->registerMyComponents();
        $this->registerHelperComponents();
        $this->registerMyTemplateDir();
        $this->registerMySnippets();


        $subscribers = array(
            new \Shopware\MilchbarImportOrders\Subscriber\ControllerPath(),
            new \Shopware\MilchbarImportOrders\Subscriber\Frontend()
        );

        foreach ($subscribers as $subscriber) {
            $this->Application()->Events()->addSubscriber($subscriber);
        }
    }

    /**
     * Registers the template directory, needed for templates in frontend an backend
     */
    public function registerMyTemplateDir()
    {
        Shopware()->Template()->addTemplateDir($this->Path() . 'Views');
    }

    /**
     * Registers the snippet directory, needed for backend snippets
     */
    public function registerMySnippets()
    {
        $this->Application()->Snippets()->addConfigDir(
            $this->Path() . 'Snippets/'
        );
    }

    public function registerHelperComponents()
    {
        $this->Application()->Loader()->registerNamespace(
            'Shopware\Components',
            $this->Path() . 'Components/'
        );
    }

    public function registerMyComponents()
    {
        $this->Application()->Loader()->registerNamespace(
            'Shopware\MilchbarImportOrders',
            $this->Path()
        );
    }

    public function onAddConsoleCommand(Enlight_Event_EventArgs $args)
    {
        $this->registerMyComponents();

        return new \Doctrine\Common\Collections\ArrayCollection(array(
            new \Shopware\MilchbarImportOrders\Commands\StatusCommand(),
        ));

    }

    private function createConfig()
    {
        $form = $this->Form();

        $form->setElement(
            'text',
            'milchbarImportOrdersSecurityToken',
            [
                'label' => 'Security token',
                'value' => password_hash(mt_rand(), PASSWORD_DEFAULT),
                'description' => 'When the plugin is reinstalled, this token also gets regenerated. So be sure to copy and save the token, so you can paste it back later. Or otherwise communicate the new token to the collecting party.'
            ]
        );
    }


}

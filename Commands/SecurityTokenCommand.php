<?php

namespace Shopware\Commands\MilchbarImportOrders;

use Shopware\Commands\ShopwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Shopware\Components\MilchbarImportOrders\Utils\ImportOrdersHelper;

class SecurityTokenCommand extends ShopwareCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('milchbar:importorders:token')
            ->setDescription('Add or change the security token')
            ->setHelp("The <info>%command.name%</info> will add or change the security token.");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
          $this->start($output);
    }

    /**
     *
     * @param OutputInterface $output
     */
    protected function start(OutputInterface $output)
    {
        $milchbarImportOrdersSecurityToken = Shopware()->Plugins()->Backend()->MilchbarImportOrders()->Config()->get('milchbarImportOrdersSecurityToken');
        if ($milchbarImportOrdersSecurityToken != '') {
          $output->writeln('<info>Security token: '.$milchbarImportOrdersSecurityToken.'</info>');
        } else {
          $output->writeln('<info>Security token: missing</info>');
          $milchbarImportOrdersSecurityToken = password_hash(mt_rand(), PASSWORD_DEFAULT);
          $output->writeln('<info>Generating token: '.$milchbarImportOrdersSecurityToken.'</info>');
          $shop = Shopware()->Models()->getRepository('Shopware\Models\Shop\Shop')->findOneBy(array('default' => true));
          $pluginManager  = Shopware()->Container()->get('shopware_plugininstaller.plugin_manager');
          $plugin = $pluginManager->getPluginByName('MilchbarImportOrders');
          $pluginManager->saveConfigElement($plugin, 'milchbarImportOrdersSecurityToken', $milchbarImportOrdersSecurityToken, $shop);
        }
    }

}

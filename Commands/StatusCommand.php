<?php

namespace Shopware\MilchbarImportOrders\Commands;


use Shopware\Components\Model\ModelManager;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Shopware\Commands\ShopwareCommand;


class StatusCommand extends ShopwareCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('milchbar:importorders:status')
            ->setDescription('Import orders from Channable')
            ->setHelp("The <info>%command.name%</info> imports orders from Channable.");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->start($output);
    }

    /**
     *
     * @param OutputInterface $output
     */
    protected function start(OutputInterface $output)
    {
        $output->writeln('<info>Import Orders from JSON</info>');
        $output->writeln('<info>class StatusCommand</info>');

        $_json = '{
            "channable_id": 1234567,
            "channel_id": 1234234,
            "channel_name": "beslist",
            "is_shipped": "not_shipped",
            "shipping": { "address_line_1": "Straatnaam 1 A", "address_line_2": "", "email": "emailadres@test.nl", "first_name": "Voornaam", "middle_name": "", "last_name": "van Achternaam", "street": "Straatnaam", "house_number": 1, "house_number_ext": "A", "country_code": "NL", "zip_code": "1234AB", "city": "Arnhem", "company": "Test company", "region": "Gelderland" },
            "billing": { "address_line_1": "Straatnaam 1 A", "address_line_2": "", "email": "emailadres@test.nl", "first_name": "Voornaam", "middle_name": "", "last_name": "van Achternaam", "street": "Straatnaam", "house_number": 1, "house_number_ext": "A", "country_code": "NL", "zip_code": "1234AB", "city": "Arnhem", "company": "Test company", "region": "Gelderland" },
            "customer": { "email": "emailadres@test.nl", "gender": "male", "middle_name": "", "mobile": "", "first_name": "Voornaam", "company": "", "last_name": "van Achternaam", "phone": "0123456789" },
            "products": [{ "reference_code": "None", "title": "Samsung Galaxy Tab P1000 beschermhoes `Tartan` - roze", "shipping": 3.0, "transaction_fee": 0.99, "delivery_period": "", "id": "11693020", "price": 6.99, "quantity": 1, "gtin": "0123456789012" }],
            "price": { "currency": "EUR", "payment": 0, "shipping": 3.0, "transaction_fee": 0.99, "subtotal": 6.99, "total": 10.98, "payment_method": "beslist" },
            "memo": "Order from Channable \n Package slip: https://www.example.com/packageslips.html"
        }';

        $importOrdersHelper = new \Shopware\MilchbarImportOrders\Components\Utils\ImportOrdersHelper(
              [
                  'json' => $_json,
              ]
        );

        $output->writeln('<info>function createOrder()</info>');
        $return = $importOrdersHelper->createOrder2();
        foreach ($return['output'] as $_output) {
          $output->writeln($_output);
        }
    }

}

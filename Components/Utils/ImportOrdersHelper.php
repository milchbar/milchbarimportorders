<?php

namespace Shopware\MilchbarImportOrders\Components\Utils;

use Shopware\Components\NumberRangeIncrementerInterface;

class ImportOrdersHelper
{
    protected $json;
    protected $order_id;
    protected $time_start;

    /**
     * @var NumberRangeIncrementerInterface
     */
    private $numberRangeIncrementer;

    /**
     * Construct
     *
     * @param array $data
     * @throws \Exception
     */
    public function __construct(array $data)
    {
        if (isset($data['json'])) {
            $this->json = $data['json'];
        }
        if (isset($data['order_id'])) {
            $this->order_id = $data['order_id'];
        }
        $this->time_start = microtime(true);

        $this->numberRangeIncrementer = Shopware()->Container()->get('shopware.number_range_incrementer');
    }

    /**
     * Prepares export
     *
     * @return array
     */
    public function createOrder()
    {
        $output[] = $this->processedTime()."\n";
        $response = new \stdClass();
        $response->validated = false;

        $_order_internal_comment = [];
        if ($this->json->channable_id == '') { $response->errors[] = 'No channable id set'; } else {
          $_order_internal_comment[] = 'channable id: '.$this->json->channable_id;
        }
        if ($this->json->channel_id == '') { $response->errors[] = 'No channel id set'; } else {
          $_order_internal_comment[] = 'channel id: '.$this->json->channel_id;
        }
        if ($this->json->channel_name == '') { $response->errors[] = 'No channel name set'; } else {
          $_order_internal_comment[] = 'channel name: '.$this->json->channel_name;
        }

        if ($this->json->customer->email == '') { $response->errors[] = 'No customer email set'; } else {
          $_customer_email = $this->json->customer->email;
        }
        $_customer_salution = 'mr';
        $_customer_firstname = $this->json->customer->first_name;
        $_customer_middle_name = $this->json->customer->middle_name != '' ? $this->json->customer->middle_name.' ' : '';
        $_customer_lastname = $_customer_middle_name.$this->json->customer->last_name;
        $_customer_company = $this->json->customer->company;
        $_customer_phone = $this->json->customer->phone;
        $_customer_mobile = $this->json->customer->mobile;

        $_customer_billing_salution = 'mr';
        $_customer_billing_firstname = $this->json->billing->first_name;
        $_customer_billing_middle_name = $this->json->billing->middle_name != '' ? $this->json->billing->middle_name.' ' : '';
        $_customer_billing_lastname = $_customer_billing_middle_name.$this->json->billing->last_name;
        $_customer_billing_address_line_1 = $this->json->billing->address_line_1;
        $_customer_billing_address_line_2 = $this->json->billing->address_line_2;
        $_customer_billing_zipcode = $this->json->billing->zip_code;
        $_customer_billing_city = $this->json->billing->city;
        $_customer_billing_country = $this->json->billing->country_code;

        $_customer_shipping_salution = 'mr';
        $_customer_shipping_firstname = $this->json->shipping->first_name;
        $_customer_shipping_middle_name = $this->json->shipping->middle_name != '' ? $this->json->shipping->middle_name.' ' : '';
        $_customer_shipping_lastname = $_customer_shipping_middle_name.$this->json->shipping->last_name;
        $_customer_shipping_address_line_1 = $this->json->shipping->address_line_1;
        $_customer_shipping_address_line_2 = $this->json->shipping->address_line_2;
        $_customer_shipping_zipcode = $this->json->shipping->zip_code;
        $_customer_shipping_city = $this->json->shipping->city;
        $_customer_shipping_country = $this->json->shipping->country_code;

        $this->json->price->payment_method = 5; // prepayment
        if ($this->json->price->payment_method == '') { $response->errors[] = 'No paymet method set'; } else {
          $_payment_method = $this->json->price->payment_method;
        }

        if (count($this->json->products) == 0) { $response->errors[] = 'No products set'; } else {
          $_products = $this->json->products;
        }


        if (count($response->errors) > 0) {
          return array('response' => $response, 'output' => $output);
        }

        $shopRepo = Shopware()->Models()->getRepository('Shopware\Models\Shop\Shop');
        $shop = $shopRepo->findOneById(1); // default shop
        $mainShop = $shop->getMain() !== null ? $shop->getMain() : $shop;
        $subShop = $shop->getMain() === null ? $shop : $shop;

        $order = new \Shopware\Models\Order\Order();
        $ordernumber = $this->numberRangeIncrementer->increment('invoice');
        $order->setNumber($ordernumber);
        $output[] = '++ordernumber '.$order->getNumber();

        $customer = new \Shopware\Models\Customer\Customer();
        $customer->setEmail($_customer_email);
        $customer->setPassword(mt_rand(10000000000000000000,99999999999999999999));
        $customer->setEncoderName('bcrypt');
        Shopware()->Models()->persist($customer);
        $customer->setActive(true);
        $customer->setAccountMode(1); // Accountless
        $customer->setPaymentId(5); // Prepayment
        $customer_group = $shop->getCustomerGroup();
        $customer->setGroup($customer_group);
        $customer->setLanguageSubShop($subShop);

        $customer_billing = new \Shopware\Models\Customer\Billing();
        $customer_billing->setCustomer($customer);
        $customer_billing->setSalutation($_customer_billing_salution);
        $customer_billing->setFirstname($_customer_billing_firstname);
        $customer_billing->setLastname($_customer_billing_lastname);
        $customer_billing->setCompany($_customer_company);
        $customer_billing->setStreet($_customer_billing_address_line_1);
        $customer_billing->setAdditionalAddressLine1($_customer_billing_address_line_2);
        $customer_billing->setZipcode($_customer_billing_zipcode);
        $customer_billing->setCity($_customer_billing_city);
        $countryRepo = Shopware()->Models()->getRepository('Shopware\Models\Country\Country');
        $country_nl = $countryRepo->findOneById(21); // NL
        $customer_billing->setCountryId($country_nl->getId());
        Shopware()->Models()->persist($customer_billing);

        $customer_shipping = new \Shopware\Models\Customer\Shipping();
        $customer_shipping->setCustomer($customer);
        $customer_shipping->setSalutation($_customer_shipping_salution);
        $customer_shipping->setFirstname($_customer_shipping_firstname);
        $customer_shipping->setLastname($_customer_shipping_lastname);
        $customer_shipping->setCompany($_customer_company);
        $customer_shipping->setStreet($_customer_shipping_address_line_1);
        $customer_shipping->setAdditionalAddressLine1($_customer_shipping_address_line_2);
        $customer_shipping->setZipcode($_customer_shipping_zipcode);
        $customer_shipping->setCity($_customer_shipping_city);
        $countryRepo = Shopware()->Models()->getRepository('Shopware\Models\Country\Country');
        $country_nl = $countryRepo->findOneById(21); // NL
        $customer_shipping->setCountryId($country_nl->getId());
        Shopware()->Models()->persist($customer_shipping);

        $customer_billing_address = new \Shopware\Models\Customer\Address();
        $customer_billing_address->setCustomer($customer);
        $customer_billing_address->setSalutation($_customer_billing_salution);
        $customer_billing_address->setFirstname($_customer_billing_firstname);
        $customer_billing_address->setLastname($_customer_billing_lastname);
        $customer_billing_address->setCompany($_customer_company);
        $customer_billing_address->setStreet($_customer_billing_address_line_1);
        $customer_billing_address->setAdditionalAddressLine1($_customer_billing_address_line_2);
        $customer_billing_address->setZipcode($_customer_billing_zipcode);
        $customer_billing_address->setCity($_customer_billing_city);
        $countryRepo = Shopware()->Models()->getRepository('Shopware\Models\Country\Country');
        $country_nl = $countryRepo->findOneById(21); // NL
        $customer_billing_address->setCountry($country_nl);
        Shopware()->Models()->persist($customer_billing_address);
        $customer->setDefaultBillingAddress($customer_billing_address);

        $customer_shipping_address = new \Shopware\Models\Customer\Address();
        $customer_shipping_address->setCustomer($customer);
        $customer_shipping_address->setSalutation($_customer_shipping_salution);
        $customer_shipping_address->setFirstname($_customer_shipping_firstname);
        $customer_shipping_address->setLastname($_customer_shipping_lastname);
        $customer_shipping_address->setCompany($_customer_company);
        $customer_shipping_address->setStreet($_customer_shipping_address_line_1);
        $customer_shipping_address->setAdditionalAddressLine1($_customer_shipping_address_line_2);
        $customer_shipping_address->setZipcode($_customer_shipping_zipcode);
        $customer_shipping_address->setCity($_customer_shipping_city);
        $countryRepo = Shopware()->Models()->getRepository('Shopware\Models\Country\Country');
        $country_nl = $countryRepo->findOneById(21); // NL
        $customer_shipping_address->setCountry($country_nl);
        Shopware()->Models()->persist($customer_shipping_address);
        $customer->setDefaultShippingAddress($customer_shipping_address);

        $customer->setSalutation($_customer_salution);
        $customer->setFirstname($_customer_firstname);
        $customer->setLastname($_customer_lastname);

        $customernumber = $this->numberRangeIncrementer->increment('user');
        $customer->setNumber($customernumber);
        $order->setCustomer($customer);

        $order_billing = new \Shopware\Models\Order\Billing();
        $order_billing->setCustomer($customer);
        $order_billing->setNumber($customernumber);
        $order_billing->setSalutation($_customer_billing_salution);
        $order_billing->setFirstname($_customer_billing_firstname);
        $order_billing->setLastname($_customer_billing_lastname);
        $order_billing->setCompany($_customer_company);
        $order_billing->setStreet($_customer_billing_address_line_1);
        $order_billing->setAdditionalAddressLine1($_customer_billing_address_line_2);
        $order_billing->setZipcode($_customer_billing_zipcode);
        $order_billing->setCity($_customer_billing_city);
        $countryRepo = Shopware()->Models()->getRepository('Shopware\Models\Country\Country');
        $country_nl = $countryRepo->findOneById(21); // NL
        $order_billing->setCountry($country_nl);
        $order->setBilling($order_billing);
        $order_shipping = new \Shopware\Models\Order\Shipping();
        $order_shipping->setCustomer($customer);
        $order_shipping->setSalutation($_customer_shipping_salution);
        $order_shipping->setFirstname($_customer_shipping_firstname);
        $order_shipping->setLastname($_customer_shipping_lastname);
        $order_shipping->setCompany($_customer_company);
        $order_shipping->setStreet($_customer_shipping_address_line_1);
        $order_shipping->setAdditionalAddressLine1($_customer_shipping_address_line_2);
        $order_shipping->setZipcode($_customer_shipping_zipcode);
        $order_shipping->setCity($_customer_shipping_city);
        $countryRepo = Shopware()->Models()->getRepository('Shopware\Models\Country\Country');
        $country_nl = $countryRepo->findOneById(21); // NL
        $order_shipping->setCountry($country_nl);
        $order->setShipping($order_shipping);


        $orderStatusRepo = Shopware()->Models()->getRepository('Shopware\Models\Order\Status');
        $order_status_open = $orderStatusRepo->findOneById(0); // open
        $order->setOrderStatus($order_status_open);
        $order_cleared_open = $orderStatusRepo->findOneById(17); // open
        $order->setPaymentStatus($order_cleared_open);

        $paymentRepo = Shopware()->Models()->getRepository('Shopware\Models\Payment\Payment');
        $payment = $paymentRepo->findOneById($_payment_method);
        $order->setPayment($payment);

        $dispatchRepo = Shopware()->Models()->getRepository('Shopware\Models\Dispatch\Dispatch');
        $dispatch = $dispatchRepo->findOneBy(array('name' => 'Channable Shipping'));
        if (!$dispatch) {
          $dispatch = new \Shopware\Models\Dispatch\Dispatch();
          $dispatch->setName('Channable Shipping');
          $dispatch->setType(0);
          $dispatch->setDescription('');
          $dispatch->setComment('');
          $dispatch->setActive(0);
          $dispatch->setPosition(0);
          $dispatch->setCalculation(0);
          $dispatch->setSurchargeCalculation(0);
          $dispatch->setTaxCalculation(0);
          $dispatch->setBindLastStock(0);
          Shopware()->Models()->persist($dispatch);
        }
        $order->setDispatch($dispatch);

        $partnerRepo = Shopware()->Models()->getRepository('Shopware\Models\Partner\Partner');
        $partner = $partnerRepo->findOneBy(array('idCode' => 'Channable'));
        if (!$partner) {
          $partner = new \Shopware\Models\Partner\Partner();
          $partner->setCompany("Channable");
          $partner->setIdCode("Channable");
          $partner->setDate(new \DateTime());
          $partner->setContact('');
          $partner->setStreet('');
          $partner->setZipCode('');
          $partner->setCity('');
          $partner->setPhone('');
          $partner->setFax('');
          $partner->setCountryName('');
          $partner->setEmail('');
          $partner->setWeb('');
          $partner->setProfile('');
          Shopware()->Models()->persist($partner);
        }
        $order->setPartner($partner);

        $order->setShop($shop);

        $order->setInvoiceAmount($this->json->price->total);
        $order->setInvoiceAmountNet($this->json->price->total);
        $order->setInvoiceShipping($this->json->price->shipping);
        $order->setInvoiceShippingNet($this->json->price->shipping);

        $orderDate = new \DateTime();
        $order->setOrderTime($orderDate);

        $order->setTransactionId(0);

        $order->setComment($this->json->memo);
        $order->setCustomerComment('');
        $order->setInternalComment(implode("\n", $_order_internal_comment));

        $order->setNet(true);
        $order->setTaxFree(false);

        $order->setTemporaryId(0);

        $order->setReferer('');
        $order->setTrackingCode('');
        $order->setLanguageIso($shop->getId());
        $shop_currency = $shop->getCurrency();
        $order->setCurrency($shop_currency->getCurrency());
        $order->setCurrencyFactor($shop_currency->getFactor());

        $order->setRemoteAddress('127.0.0.1');

        $order_details = [];
        foreach ($_products as $_product) {
          $articleDetailRepo = Shopware()->Models()->getRepository('Shopware\Models\Article\Detail');
          $article_detail = $articleDetailRepo->findOneById(118);
          $article = $article_detail->getArticle();
          $article_tax = $article->getTax();

          $order_detail = new \Shopware\Models\Order\Detail();
          $order_detail->setArticleId($article_detail->getId());
          $order_detail->setTax($article_tax);
          $order_detail->setTaxRate($article_tax->getTax());
          $orderDetailStatusRepo = Shopware()->Models()->getRepository('Shopware\Models\Order\DetailStatus');
          $order_detail_status_open = $orderDetailStatusRepo->findOneById(0); // open
          $order_detail->setStatus($order_detail_status_open);
          $order_detail->setNumber($ordernumber);
          $order_detail->setArticleNumber($article_detail->getNumber());
          $order_detail->setPrice($this->json->products[0]->price);
          $order_detail->setQuantity($this->json->products[0]->quantity);
          $order_detail->setArticleName($this->json->products[0]->title);

          $order_details[] = $order_detail;
        }
        $order->setDetails($order_details);


        Shopware()->Models()->persist($order);
        if (count($response->errors) == 0) {
          Shopware()->Models()->flush();
          $response->validated = true;
        }

        if ($response->validated) { // An successfull response example
          $response->order_id = $order->getId();
        } else { // An error response example
        }

        $output[] = $this->processedTime()."\n";

        return array('response' => $response, 'output' => $output);
    }

    /**
     * Prepares export
     *
     * @return array
     */
    public function statusOrder()
    {
        $output[] = $this->processedTime()."\n";
        $response = new \stdClass();
        $response->validated = false;

        if ($this->order_id == '') { $response->errors[] = 'No order id set'; } else {
        }

        if (count($response->errors) > 0) {
          return array('response' => $response, 'output' => $output);
        }

        $orderRepo = Shopware()->Models()->getRepository('Shopware\Models\Order\Order');
        $order = $orderRepo->findOneById($this->order_id);

        $order_status = $order->getOrderStatus();


        if (count($response->errors) == 0) {
          $response->validated = true;
        }

        if ($response->validated) {
          $response->order_id = $order->getId();
          $response->order_status = $order_status->getName();
          $response->order_tracking_code = $order->getTrackingCode();
        } else { // An error response example
        }

        $output[] = $this->processedTime()."\n";

        return array('response' => $response, 'output' => $output);
    }

    protected function processedTime()
    {
        $time_interval = microtime(true);
        $time_difference = $time_interval - $this->time_start;
        return number_format($time_difference, 2);
    }

}

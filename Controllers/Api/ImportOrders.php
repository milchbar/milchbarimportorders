<?php

class Shopware_Controllers_Api_ImportOrders extends Shopware_Controllers_Api_Rest
{
    /**
     * @var Shopware\Components\Api\Resource\ImportOrders
     */
    // protected $resource;

    public function init()
    {
    }

    /**
     * Get list of entities
     *
     * GET /api/importorders/
     */
    public function indexAction()
    {
    }

    /**
     * Get one entity
     *
     * GET /api/importorders/{id}
     */
    public function getAction()
    {
    }

    /**
     * Create new entity
     *
     * POST /api/importorders
     */
    public function postAction()
    {
    }

    /**
     * Update entity
     *
     * PUT /api/alltest/{id}
     */
    public function putAction()
    {
    }

    /**
     * Delete entity
     *
     * DELETE /api/alltest/{id}
     */
    public function deleteAction()
    {
    }
}

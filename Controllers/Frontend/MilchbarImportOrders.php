<?php

/**
 * Frontend controller
 */
class Shopware_Controllers_Frontend_MilchbarImportOrders extends Enlight_Controller_Action
{


    public function init()
    {
        Shopware()->Plugins()->Controller()->ViewRenderer()->setNoRender();
    }

    public function indexAction()
    {
        $request = $this->Request();
    }
    public function insertAction()
    {
      $request = $this->Request();
      $_debug = $request->getParam('debug') ? true : false;

      if ($_debug) {
        echo "<pre>";
      }
      
      if ($request->getParam('token')) {
        $_sec_token = $request->getParam('token');
        $milchbarImportOrdersSecurityToken = Shopware()->Plugins()->Backend()->MilchbarImportOrders()->Config()->get('milchbarImportOrdersSecurityToken');
        if ($_sec_token === $milchbarImportOrdersSecurityToken) {
        } else {
          die("token invalid");
        }
      } else {
        die("token missing");
      }

      echo "<form method=\"post\">JSON >> <input type=\"text\" name=\"json\" value=\"\"  size=\"200\"><input type=\"submit\" value=\"Submit\">
      <input type=\"text\" name=\"debug\" value=\"1\" size=\"10\" ><input type=\"text\" name=\"token\" value=\"\$2y$10\$Qb0KcbGdGm8e1bUzuZrgueytwkdzU42rnqyqe0SH5yTLkYI5e8Iby\" size=\"80\" ><input type=\"text\" name=\"__csrf_token\" value=\"".$_COOKIE['__csrf_token-8']."\" size=\"50\" /></form>";


      $_json = $request->getParam('json') != '' ? json_decode($request->getParam('json')) : false;
      if ($_json) {
        print_r($_json);

        $importOrdersHelper = new \Shopware\MilchbarImportOrders\Components\Utils\ImportOrdersHelper(
              [
                  'json' => $_json,
              ]
        );
        $return = $importOrdersHelper->createOrder();

        if ($_debug) {
          foreach ($return['output'] as $_output) {
            echo $_output."\n";
          }
        }
      }

      echo json_encode($return['response']);

echo "\n<hr>copy this json in the input field<br />";
echo '{
    "channable_id": 1234567,
    "channel_id": 1234234,
    "channel_name": "beslist",
    "is_shipped": "not_shipped",
    "shipping": { "address_line_1": "Straatnaam 1 A", "address_line_2": "", "email": "emailadres@test.nl", "first_name": "Voornaam", "middle_name": "", "last_name": "van Achternaam", "street": "Straatnaam", "house_number": 1, "house_number_ext": "A", "country_code": "NL", "zip_code": "1234AB", "city": "Arnhem", "company": "Test company", "region": "Gelderland" },
    "billing": { "address_line_1": "Straatnaam 1 A", "address_line_2": "", "email": "emailadres@test.nl", "first_name": "Voornaam", "middle_name": "", "last_name": "van Achternaam", "street": "Straatnaam", "house_number": 1, "house_number_ext": "A", "country_code": "NL", "zip_code": "1234AB", "city": "Arnhem", "company": "Test company", "region": "Gelderland" },
    "customer": { "email": "emailadres@test.nl", "gender": "male", "middle_name": "", "mobile": "", "first_name": "Voornaam", "company": "", "last_name": "van Achternaam", "phone": "0123456789" },
    "products": [{ "reference_code": "None", "title": "Samsung Galaxy Tab P1000 beschermhoes `Tartan` - roze", "shipping": 3.0, "transaction_fee": 0.99, "delivery_period": "", "id": "11693020", "price": 6.99, "quantity": 1, "gtin": "0123456789012" }],
    "price": { "currency": "EUR", "payment": 0, "shipping": 3.0, "transaction_fee": 0.99, "subtotal": 6.99, "total": 10.98, "payment_method": "beslist" },
    "memo": "Order from Channable \n Package slip: https://www.example.com/packageslips.html"
}';

    }

}

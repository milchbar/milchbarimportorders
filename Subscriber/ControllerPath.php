<?php

namespace Shopware\MilchbarImportOrders\Subscriber;

use Enlight\Event\SubscriberInterface;

class ControllerPath implements SubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
                        'Enlight_Controller_Dispatcher_ControllerPath_Frontend_MilchbarImportOrders' => 'onGetControllerPathFrontend',            'Enlight_Controller_Dispatcher_ControllerPath_Api_ImportOrders' => 'getApiControllerImportOrders'        );
    }


    public function getApiControllerImportOrders(\Enlight_Event_EventArgs $args)
    {
        return __DIR__ . '/../Controllers/Api/ImportOrders.php';
    }



    /**
     * Register the frontend controller
     *
     * @param   \Enlight_Event_EventArgs $args
     * @return  string
     * @Enlight\Event Enlight_Controller_Dispatcher_ControllerPath_Frontend_MilchbarFrontend     */
    public function onGetControllerPathFrontend(\Enlight_Event_EventArgs $args)
    {
        return __DIR__ . '/../Controllers/Frontend/MilchbarImportOrders.php';
    }
}
